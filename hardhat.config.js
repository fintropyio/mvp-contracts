require("@nomiclabs/hardhat-waffle");
require("hardhat-erc1820");
require("hardhat-watcher");
require("solidity-coverage");

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async () => {
  const accounts = await ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});


let infura_id = '4b2a5df22db2487da1d92b06ea784c25'

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    compilers: [
      {version: "0.8.4"},
      {version: "0.6.6"},
    ]
  },
  defaultNetwork: "localhost",
  networks: {
    hardhat: {
    },
    localhost: {
      url: "http://127.0.0.1:8545",
    },

    // Mtic mainnet fork (quickswap).
    // npx hardhat node --fork https://polygon-mainnet.infura.io/v3/4b2a5df22db2487da1d92b06ea784c25 --fork-block-number 16245998 --hostname 0.0.0.0
    // npx hardhat run scripts/deploy/deploy.js --network matic_fork
    matic_fork: {
      url: "http://127.0.0.1:8545",
    },

    // Polygon mainnet (quickswap)
    matic: {
      url: "https://polygon-mainnet.infura.io/v3/" + infura_id,
    },

    // Rinkeby testnet (uniswap)
    rinkeby: {
      url: "https://rinkeby.infura.io/v3/" + infura_id,
    },
  },
  watcher: {
    compilation: {
      tasks: ["compile"],
    }
  },
};

