# Fintropy contracts

## Setup
Node version should be above ```v16.2.0```
```shell
git submodule update --init --recursive
npm install

```

## Configuration &nbsp; ```/config```

**Mainnet**:

```matic.json5```: &nbsp;&nbsp; Polygon mainnet [https://polygonscan.com](https://polygonscan.com)

**Mainnet local fork**:

```matic_fork.json5```: &nbsp;&nbsp; Polygon mainnet local fork


## Deployment

Deployment script takes a configuration file specified by ```--network``` parameter, deploys contracts if needed and builds a config file for [FintropyWeb3](https://gitlab.com/blocks-and-chains/fint-web3) library.

The deployment script expects ```FINT_HOST_PRIVATE_KEY``` env variable to be defined.

Dev host ```0x6d29C1C21402e48869c7A4f1930e03cf06C27A6b``` (private key ```3e0c41f663550741fd7ce849204fc55adc7dd3f986b2646652489ac0d1944b75```).

Test ERC20 Tokens and corresponding pairs may be deployed if specified in the configuration file.

See configuration files for details.

**Mainnet**:
```shell
npx hardhat run scripts/deploy/deploy.js --network matic
```

**Mainnet local fork**:

```shell
npx hardhat node --fork https://polygon-mainnet.infura.io/v3/<INFURA_ID> --fork-block-number <BLOCK_NUMBER> --hostname 0.0.0.0
npx hardhat run scripts/deploy/deploy.js --network matic_fork
```

Dev INFURA_ID ```4b2a5df22db2487da1d92b06ea784c25```.
Dev BLOCK_NUMBER ```16245998```.


## Graph 

#### Generate code and build

```shell
node subgraph/data/update.js # copy abi from artifacts folder
cd subgraph/data/
yarn  && yarn codegen # install dependencies and generate code
yarn build # ensure no errors occur

```

#### Deploy to the [Hosted Service](https://thegraph.com/docs/quick-start#hosted-service)

Graph explorer: [fintropy/data](https://thegraph.com/explorer/subgraph/fintropy/data)

```
yarn deploy --access-token <account's access token>
```


## Dev GUI &nbsp; ```/frontend```

Front-end client application. Demonstrates [FintropyWeb3](https://gitlab.com/blocks-and-chains/fint-web3) package usage.

Requires the configuration file ```/frontent/config/config.json``` composed by deployment script.

Run
```
npm install
```
```
npm run serve
```
inside ```/frontent``` folder to start the app.


## Troubleshooting

- ```Internal JSON-RPC error. Received invalid block tag A. Latest block number is B```

    when using mainnet fork network and Metamask

    **Solution**:

    Reset Account in Metamask settings

- ```Nonce is too high```

    when using mainnet fork network and Metamask

    **Solution**:

    Reset Account in Metamask settings
