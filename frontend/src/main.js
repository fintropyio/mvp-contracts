import { createApp } from 'vue'
import App from './App.vue'
import store from './store'

import PrimeVue from 'primevue/config';

import "primevue/resources/themes/mdc-dark-deeppurple/theme.css"
import "primevue/resources/primevue.min.css"
import "primeicons/primeicons.css"
import 'primeflex/primeflex.css';

import InputText from "primevue/inputtext";
import TabView from "primevue/tabview";
import TabPanel from "primevue/tabpanel";
import Button from 'primevue/button';
import Fieldset from 'primevue/fieldset';
import Card from 'primevue/card';
import Accordion from 'primevue/accordion';
import AccordionTab from 'primevue/accordiontab';
import Tooltip from 'primevue/tooltip';
import InputNumber from 'primevue/inputnumber';
import DataView from 'primevue/dataview';
import ToastService from 'primevue/toastservice';
import Toast from 'primevue/toast';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';
import Dialog from 'primevue/dialog';
import ConfirmPopup from 'primevue/confirmpopup';
import ConfirmationService from 'primevue/confirmationservice';
import Checkbox from 'primevue/checkbox';
import Slider from 'primevue/slider';
import Textarea from 'primevue/textarea';
import Dropdown from 'primevue/dropdown';
import Divider from 'primevue/divider';

const app = createApp(App).use(store).use(PrimeVue)

app.component("InputText", InputText);
app.component("TabView", TabView);
app.component("TabPanel", TabPanel);
app.component("Button", Button);
app.component("Fieldset", Fieldset);
app.component("Card", Card);
app.component("Accordion", Accordion);
app.component("AccordionTab", AccordionTab);
app.component('InputNumber', InputNumber);
app.component('DataView', DataView);
app.component('Toast', Toast);
app.component('DataTable', DataTable);
app.component('Column', Column);
app.component('ColumnGroup', ColumnGroup);
app.component('Dialog', Dialog);
app.component('ConfirmPopup', ConfirmPopup);
app.component('Checkbox', Checkbox);
app.component('Slider', Slider);
app.component('Textarea', Textarea);
app.component('Dropdown', Dropdown);
app.component('Divider', Divider);

app.directive('tooltip', Tooltip);
app.use(ToastService);
app.use(ConfirmationService);

app.mount('#app')