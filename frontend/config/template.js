

module.exports = {
  network: '$NETWORK',
  chain_id: '$CHAIN_ID',

  contract_addresses: {
    'PortController': '$PORT_CONTROLLER_ADDRESS',
    'PortFactory': '$PORT_FACTORY_ADDRESS',
    'PortTokenUtils': '$PORT_TOKEN_UTILS_ADDRESS',
    'SwapRouter': '$SWAP_ROUTER_ADDRESS',
  },
  abi: '$ABI',
  tokens: '$TOKENS',
  owner_address: '$OWNER_ADDRESS',

  rpc: {
    1: 'https://mainnet.infura.io/v3/$INFURA_ID',
    4: 'https://rinkeby.infura.io/v3/$INFURA_ID',
    5: 'https://goerli.infura.io/v3/$INFURA_ID',
    137: 'https://polygon-mainnet.infura.io/v3/$INFURA_ID',
    80001: 'https://polygon-mumbai.infura.io/v3/$INFURA_ID'
  },

  subgraph_endpoint: '$SUBGRAPH_ENDPOINT',

  port_token_name_length: {
    min: 1,
    max: 32
  },
  port_token_symbol: 'PORT',
  port_token_decimals: '18',

  // Development
  owner_private_key: '$OWNER_PRIVATE_KEY',
  faucet: {
    base_token_amount: '$FAUCET_BASE_TOKEN_AMOUNT',
    base_token_decimals: '$FAUCET_BASE_TOKEN_DECIMALS',
    token_amount: '$FAUCET_TOKEN_AMOUNT',
    token_decimals: '$FAUCET_TOKEN_DECIMALS'
  }
}