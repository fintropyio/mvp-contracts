//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;
import "hardhat/console.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

/*
*   Represents porfolio token
*/
contract PortToken is ERC20 {
    using SafeERC20 for IERC20;
    address private controller; // Controller contract address
    address public manager; // Manager of contract

    address[] public intTokens; // addresses of internal ERC20 components
    mapping(address=>uint256) public intAmts; // number of component base units in 10^18 of portfolio token units

    modifier onlyController() {
        require(msg.sender==controller, "CONTROLLER CALL ONLY");
        _;
    }

    constructor(
        address _controller,
        address _manager,
        string memory _name,
        string memory _symbol,
        address[] memory _intTokens,
        uint256[] memory _intAmts
    ) ERC20(_name, _symbol) {
        manager=_manager;
        controller=_controller;

        intTokens=_intTokens;

        for (uint256 i=0;i<_intTokens.length;i++) {
            intAmts[intTokens[i]]=_intAmts[i];
        }
    }

    function intTokensLength() external view returns(uint256) {
        return intTokens.length;
    }

    function mint(address _to, uint256 _amt) external onlyController() {
        _mint(_to, _amt);
    }

    function burn(address _from, uint256 _amt) external onlyController() {
        _burn(_from, _amt);
    }

    function transferIntToken(address _intToken, address _to, uint256 _amt) external onlyController() {
        IERC20(_intToken).safeTransfer(_to, _amt);
    }
}