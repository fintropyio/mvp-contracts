//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

contract WrappedToken is ERC20 {
    uint8 immutable _decimals;
    IERC20 immutable public _baseToken;
    using SafeERC20 for IERC20;

    constructor(string memory name, string memory symbol, uint8 decimals_, address baseTokenAddr) ERC20(name, symbol) {
        _decimals = decimals_;
        _baseToken = IERC20(baseTokenAddr);
    }

    function decimals() public view virtual override returns (uint8) {
        return _decimals;
    }

    function deposit(uint256 amount) external {
        _baseToken.safeTransferFrom(msg.sender, address(this), amount);
        _mint(msg.sender, amount);
    }

    function withdraw(uint256 amount) external {
        _baseToken.safeTransfer(msg.sender, amount);
        _burn(msg.sender, amount);
    }
}
