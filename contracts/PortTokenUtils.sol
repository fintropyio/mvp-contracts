//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "./PortToken.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";

/*
 * Misc tools library
 */

library PortTokenUtils {
    /// @notice computes amount of internal tokens, contained in provided amount of portfolio token
    /// @param self address of PortToken instance
    /// @param amount amount of PortToken base units
    /// @return array of internal tokens and array of corresponding amounts of base units
    function getIntAmount(address self, uint256 amount)
        public
        view
        returns (address[] memory, uint256[] memory)
    {
        PortToken token = PortToken(self);
        address[] memory intTokens = new address[](token.intTokensLength());
        uint256[] memory intAmounts = new uint256[](token.intTokensLength());
        for (uint256 i = 0; i < token.intTokensLength(); i++) {
            intTokens[i] = token.intTokens(i);
            intAmounts[i] =
                (token.intAmts(token.intTokens(i)) * amount) /
                (10**token.decimals());
        }
        return (intTokens, intAmounts);
    }

    /// @notice estimates portfolio token price in other token
    /// @param self address of PortToken instance
    /// @param amount amount of PortToken base units
    /// @param otherToken address of other token
    /// @param swapRouter address of IUniswapV2Router02 swap router
    /// @param directionOut swap direction true: port token => other token, false: other token => port token
    /// directionOut true: returns maximum amt of other tokens if swaped for amount of portfolio token
    /// directionOut false: returns minimum amt of other tokens to swap to internal tokens, needed for creation of amount portfolio tokens
    function getOtherTokenAmount(
        address self,
        uint256 amount,
        address otherToken,
        address swapRouter,
        bool directionOut
    ) public view returns (uint256, uint256[] memory) {
        address[] memory intTokens;
        uint256[] memory intAmount;
        
        (intTokens, intAmount) = getIntAmount(self, amount);
        uint256[] memory otherAmountPerIntToken = new uint256[](intTokens.length);

        uint256 otherAmountTotal = 0;
        address[] memory path = new address[](2);

        for (uint256 i; i < intTokens.length; i++) {
            if (otherToken == intTokens[i]) {
                otherAmountPerIntToken[i] = intAmount[i];
                otherAmountTotal += intAmount[i];
            } else {
                if (directionOut) {
                    path[0] = intTokens[i];
                    path[1] = otherToken;
                    otherAmountPerIntToken[i] = IUniswapV2Router02(swapRouter).getAmountsOut(intAmount[i], path)[1];
                } else {
                    path[0] = otherToken;
                    path[1] = intTokens[i];
                    otherAmountPerIntToken[i] = IUniswapV2Router02(swapRouter).getAmountsIn(intAmount[i], path)[0];
                }

                otherAmountTotal+= otherAmountPerIntToken[i];
            }
        }

        return (otherAmountTotal, otherAmountPerIntToken);
    }
}
