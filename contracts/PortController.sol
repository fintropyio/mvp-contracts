//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "./PortToken.sol";
import "./PortTokenUtils.sol";
import "./WrappedToken.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

contract PortController {
    address immutable public routerAddr;
    
    using PortTokenUtils for address;
    using SafeERC20 for IERC20;
    using SafeERC20 for PortToken;
    using SafeERC20 for WrappedToken;

    constructor(address _routerAddr) {
        routerAddr = _routerAddr;
    }

    function issue(address _portAddr, uint256 _amt) external {
        PortToken token = PortToken(_portAddr);
        
        address[] memory intTokens;
        uint256[] memory intAmount;
        (intTokens, intAmount)= _portAddr.getIntAmount(_amt);
 
        for (uint256 i;i<intTokens.length;i++) {
            IERC20(intTokens[i]).transferFrom(msg.sender, _portAddr, intAmount[i]);
        }

        token.mint(msg.sender, _amt);
    }

    
    function redeem(address _portAddr, uint _amt) external {
        PortToken token = PortToken(_portAddr);
        address[] memory intTokens;
        uint256[] memory intAmount;
        (intTokens, intAmount)= _portAddr.getIntAmount(_amt);
        
        for (uint256 i; i<intTokens.length; i++) {
            token.transferIntToken(intTokens[i], msg.sender, intAmount[i]);
        }

        token.burn(msg.sender, _amt);       
    }

    //before this call controller should already have otherTokenAmountTotal of other token balance
    function _issueFromOtherToken(address to, address _portAddr, address _otherTokenAddr, uint256 _amount, uint256[] memory otherTokenAmount, uint deadline) private {
        if (deadline==0) {
            deadline=block.timestamp;
        }
 
        address[] memory intTokens;
        uint256[] memory intAmount;
        (intTokens, intAmount)= _portAddr.getIntAmount(_amount);
        address[] memory path = new address[](2);

        for (uint256 i=0; i<intTokens.length; i++) {
            path[0] = _otherTokenAddr;
            path[1] = intTokens[i];
            if (path[0]==path[1]) {
                IERC20(_otherTokenAddr).safeTransfer(_portAddr, intAmount[i]);    
            } else {
                IUniswapV2Router02(routerAddr).swapTokensForExactTokens(
                    intAmount[i],
                    otherTokenAmount[i],
                    path,
                    _portAddr,
                    deadline
                );
            }
        }

        PortToken(_portAddr).mint(to, _amount);
    }

    function issueFromOtherToken(address _portAddr, address _otherTokenAddr, uint256 _amount, uint deadline) external {     
        uint256 otherTokenAmountTotal;
        uint256[] memory otherTokenAmount;
        (otherTokenAmountTotal, otherTokenAmount) = _portAddr.getOtherTokenAmount(_amount,_otherTokenAddr,routerAddr,false);

        IERC20(_otherTokenAddr).safeTransferFrom(msg.sender, address(this), otherTokenAmountTotal);
        IERC20(_otherTokenAddr).safeIncreaseAllowance(routerAddr, otherTokenAmountTotal);

        _issueFromOtherToken(msg.sender, _portAddr, _otherTokenAddr, _amount, otherTokenAmount, deadline);
    }

    function issueFromWrappedToken(address _portAddr, address _wrappedTokenAddr, uint256 _amount, uint deadline) external {   
        WrappedToken wtoken = WrappedToken(_wrappedTokenAddr);
        address _otherTokenAddr = address(wtoken._baseToken());
        
        uint256 otherTokenAmountTotal;
        uint256[] memory otherTokenAmount;
        (otherTokenAmountTotal, otherTokenAmount) = _portAddr.getOtherTokenAmount(_amount,_otherTokenAddr,routerAddr,false);
        
        wtoken.safeTransferFrom(msg.sender, address(this), otherTokenAmountTotal);
        wtoken.withdraw(otherTokenAmountTotal);
        IERC20(_otherTokenAddr).safeIncreaseAllowance(routerAddr, otherTokenAmountTotal);

        _issueFromOtherToken(msg.sender, _portAddr, _otherTokenAddr, _amount, otherTokenAmount, deadline);
    }

    function _redeemToOtherToken(address _to, address _from, address _portAddr, address _otherTokenAddr, uint256 _amount, uint256[] memory otherTokenAmount, uint deadline) private{ 
        address to = _to;
        if (deadline==0) {
            deadline=block.timestamp;
        }
        PortToken portToken = PortToken(_portAddr);
        IUniswapV2Router02 router = IUniswapV2Router02(routerAddr);
        
        

        require(
            portToken.allowance(_from, address(this)) >= _amount, 
            "PC:INSUFFICIENT_ALLOWANCE"
        );
        
        require(
            portToken.balanceOf(_from) >= _amount, 
            "PC:INSUFFICIENT_BALANCE"
        );
        

        address[] memory intTokens;
        uint256[] memory intAmount;
        (intTokens, intAmount)= _portAddr.getIntAmount(_amount);

        address[] memory path = new address[](2);
        for (uint256 i=0; i<intTokens.length; i++) {
            path[0] = intTokens[i];
            path[1] = _otherTokenAddr;
            
            if (path[0]==path[1]) {
                portToken.transferIntToken(intTokens[i], to, intAmount[i]);
            } else {
                portToken.transferIntToken(intTokens[i], address(this), intAmount[i]);
                IERC20(intTokens[i]).safeIncreaseAllowance(routerAddr, intAmount[i]);

                router.swapExactTokensForTokens(
                    intAmount[i],
                    otherTokenAmount[i],
                    path,
                    to,
                    deadline
                );
            }
        }

        portToken.burn(_from, _amount);
    }

    function redeemToOtherToken(address _portAddr, address _otherTokenAddr, uint256 _amount, uint deadline) external {  
        uint256 otherTokenAmountTotal;
        uint256[] memory otherTokenAmount;
        (otherTokenAmountTotal, otherTokenAmount) = _portAddr.getOtherTokenAmount(_amount,_otherTokenAddr,routerAddr,true);

        _redeemToOtherToken(msg.sender, msg.sender, _portAddr, _otherTokenAddr, _amount, otherTokenAmount, deadline);
    }


    function redeemToWrappedToken(address _portAddr, address _wrappedTokenAddr, uint256 _amount, uint deadline) external { 
        WrappedToken wtoken = WrappedToken(_wrappedTokenAddr);
        address _otherTokenAddr = address(wtoken._baseToken());

        uint256 otherTokenAmountTotal;
        uint256[] memory otherTokenAmount;
        (otherTokenAmountTotal, otherTokenAmount) = _portAddr.getOtherTokenAmount(_amount,_otherTokenAddr,routerAddr,true);

        _redeemToOtherToken(address(this), msg.sender, _portAddr, _otherTokenAddr, _amount, otherTokenAmount, deadline);

        IERC20(_otherTokenAddr).safeIncreaseAllowance(_wrappedTokenAddr, otherTokenAmountTotal);
        wtoken.deposit(otherTokenAmountTotal);
        wtoken.safeTransfer(msg.sender, otherTokenAmountTotal);
    }

}
