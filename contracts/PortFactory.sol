//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./PortToken.sol";

/*
 *   Constructs Porfolio tokens
 *   And holds list of all tokens
 */

contract PortFactory is Ownable {
    address public controller;
    address[] public portTokens;

    event CreatedPortToken(
        address tokenAddr,
        address manager,
        string name,
        string symbol,
        address[] internalTokenAddresses,
        uint256[] internalTokenAmounts
    );

    constructor(address _controller) {
        controller = _controller;
    }

    function createPortToken(
        string memory _name,
        string memory _symbol,
        address[] memory _internalTokenAddresses,
        uint256[] memory _amounts
    ) public returns (address) {
        PortToken token = new PortToken(
            controller,
            msg.sender,
            _name,
            _symbol,
            _internalTokenAddresses,
            _amounts
        );

        address addr = address(token);
        portTokens.push(addr);

        emit CreatedPortToken(addr, msg.sender, _name, _symbol, _internalTokenAddresses, _amounts);
        return addr;
    }

    function portTokensLength() external view returns (uint256) {
        return portTokens.length;
    }
}
