const { expect } = require("chai");
const { BigNumber } = require("ethers");
const { waffle } = require("hardhat");
const provider = waffle.provider;

describe("WrappedToken", function () {
    const testCap=BigNumber.from("10").pow(18).mul(100);
    const testAmt=testCap.div(10);
    let otherToken;
    let wrapToken;
    let signers;


    this.beforeEach(async function () {
        signers = await ethers.getSigners();
        otherToken=await (await ethers.getContractFactory("VanilaERC20")).deploy("Other token ", "OT", testCap,6);
        await otherToken.deployed();

        wrapToken=await (await ethers.getContractFactory("WrappedToken")).deploy("Fint token ", "Fint", 6, otherToken.address);
        await wrapToken.deployed();
    });


    it("should fail if no approval",async function(){
        await expect(wrapToken.deposit(testAmt)).to.be.revertedWith("ERC20: transfer amount exceeds allowance")
    });

    it("should fail if no balance",async function(){
        await otherToken.approve(wrapToken.address, testCap.mul(2));
        expect(await otherToken.allowance(await signers[0].getAddress(),wrapToken.address)).to.eq(testCap.mul(2));

        await expect(wrapToken.deposit(testCap.mul(2))).to.be.revertedWith("ERC20: transfer amount exceeds balance")
        await expect(wrapToken.withdraw(testAmt)).to.be.revertedWith("ERC20: transfer amount exceeds balance")
    });
    

    it("should deposit and withdrawal", async function () {       
        await otherToken.approve(wrapToken.address, testAmt);
        await wrapToken.deposit(testAmt);
        expect(await wrapToken.balanceOf(await signers[0].getAddress())).to.eq(testAmt);
        expect(await otherToken.balanceOf(await signers[0].getAddress())).to.eq(testCap.sub(testAmt));

        await wrapToken.withdraw(testAmt);
        expect(await wrapToken.balanceOf(await signers[0].getAddress())).to.eq(0);
        expect(await otherToken.balanceOf(await signers[0].getAddress())).to.eq(testCap);
    });
});