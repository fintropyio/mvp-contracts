const { expect } = require("chai");
const { waffle } = require("hardhat");
const provider = waffle.provider;
const { portTokenFixtures } = require("./fixtures/port.js");

describe("PortFactory", function () {
    let factory;
    let baseToken;
    let otherTokens = [];
    let signers;

    this.beforeEach(async function () {
        signers = await ethers.getSigners();

        Factory = await ethers.getContractFactory("PortFactory");
        factory = await Factory.deploy(ethers.constants.AddressZero);
        await factory.deployed();

        baseToken = await (
            await ethers.getContractFactory("ERC20")
        ).deploy("Base token", "BAS");
        await baseToken.deployed();

        otherTokensContract = await ethers.getContractFactory("ERC20");
        for (i = 0; i < 4; i++) {
            otherTokens.push(await otherTokensContract.deploy("Other token " + i, "OT" + i));
        }
    });


    it("Should create port token instance", async function () {
        await expect(factory.createPortToken(
            "Test token",
            "TST1",
            [otherTokens[0].address, otherTokens[1].address],
            [1, 1]
        )).to.emit(factory, "CreatedPortToken");

        expect(await factory.portTokensLength()).to.eq(1);

        portAddr = (await factory.portTokens(0));
        expect(portAddr).to.be.properAddress;

        portToken = await ethers.getContractAt("PortToken", portAddr);
        expect(await portToken.manager()).to.equal(signers[0].address);
    });

    // moved offchain
    // it("Should return user balance", async function () {
    //     ({
    //         signers,
    //         factory, weth, router, tokenData, tokens, pairs,
    //         portUtils, portController, portFactory,
    //         portAddr, portToken, intTokenData, intTokens, intTokenAmounts
    //     } = await waffle.loadFixture(portTokenFixtures));
    //     let balance = ethers.utils.parseUnits("100",18);
    //     let user = signers[2];

    //     let balanceUSDC = await tokens["USDC"].balanceOf(await user.getAddress());
    //     await tokens["USDC"].connect(user).approve(portController.address, balanceUSDC);
    //     await portController.connect(user).issueFromOtherToken(portAddr, tokens["USDC"].address, balance, 0);
        
    //     let res = await portFactory.userPortTokens(await user.getAddress());
    //     expect(res).deep.to.eq([[portAddr],[balance]]);

    //     await portToken.connect(user).transfer(await signers[1].getAddress(),balance.div(2));
        
    //     expect(
    //         await portFactory.userPortTokens(await user.getAddress())
    //     ).deep.to.eq([[portAddr],[balance.div(2)]]);
    //     expect(
    //         await portFactory.userPortTokens(await signers[1].getAddress())
    //     ).deep.to.eq([[portAddr],[balance.div(2)]]);

    //     expect(
    //         await portFactory.userPortTokens(await signers[0].getAddress())
    //     ).deep.to.eq([[],[]]);

    // })


});