const { waffle } = require("hardhat");
const UniswapV2Factory = require('@uniswap/v2-core/build/UniswapV2Factory.json')
const UniswapV2Router = require('@uniswap/v2-periphery/build/UniswapV2Router02.json');
const WETH = require("@uniswap/v2-periphery/build/WETH9.json");
const { getContractFactory } = require("@nomiclabs/hardhat-ethers/types");


exports.swapFixtures = async function () {
    signers = await ethers.getSigners();

    const factory = await waffle.deployContract(signers[0], UniswapV2Factory, [await signers[0].getAddress()]);
    const weth = await waffle.deployContract(signers[0], WETH, []);
    const router = await waffle.deployContract(signers[0], UniswapV2Router, [factory.address, weth.address]);

    const tokenData = {
        USDC: { dec: 6, total: ethers.utils.parseUnits("1", 6 + 6) },
        DAI: { dec: 18, total: ethers.utils.parseUnits("1", 18 + 6) },
        UNI: { dec: 18, total: ethers.utils.parseUnits("2", 18 + 6) },
        DOGE: { dec: 18, total: ethers.utils.parseUnits("0.5", 18 + 6) }
    };

    let part = Math.round(Object.keys(tokenData).length * 5);
    let tokens = {};
    let ERC20 = await ethers.getContractFactory("VanilaERC20");

    for ([key, item] of Object.entries(tokenData)) {
        tokens[key] = await ERC20.deploy(key, key, item.total, item.dec);
        await tokens[key].deployed();

        let amt = item.total.div(part);
        for(i=2;i<5;i++) {
            await tokens[key].transfer(await signers[i].getAddress(), amt);
        }
    }

    let pairs = [];
    let usdcAmt = tokenData["USDC"].total.div(part);

    for ([key, item] of Object.entries(tokens)) {
        pairs.push(await factory.createPair(weth.address, item.address));

        let amt = tokenData[key].total.div(part);
        await item.approve(router.address, amt);
        await router.addLiquidityETH(
            item.address,
            amt,
            0,
            0,
            await signers[0].getAddress(),
            Math.round(Date.now() / 1000 + 3600),
            { value: ethers.utils.parseEther("10") }
        );

        if (key != "USDC") {
            pairs.push(await factory.createPair(item.address, tokens["USDC"].address));
            await tokens["USDC"].approve(router.address, usdcAmt);
            await item.approve(router.address, amt)

            await router.addLiquidity(
                tokens["USDC"].address,
                item.address,
                usdcAmt,
                amt,
                0,
                0,
                await signers[0].getAddress(),
                Math.round(Date.now() / 1000 + 3600)
            );
        }
    }
    
    return { signers, factory, weth, router, tokenData, tokens, pairs };
}