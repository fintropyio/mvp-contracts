const { waffle } = require("hardhat");
const { swapFixtures } = require("./uniswapv2.js");

exports.portFixtures = async function () {
    const { signers, factory, weth, router, tokenData, tokens, pairs } = await waffle.loadFixture(swapFixtures);

    let Utils = await ethers.getContractFactory("PortTokenUtils");
    portUtils = await Utils.deploy();
    await portUtils.deployed();

    Controller = await ethers.getContractFactory("PortController", {
        libraries: { PortTokenUtils: portUtils.address }
    });
    portController = await Controller.deploy(router.address);
    await portController.deployed();

    Factory = await ethers.getContractFactory("PortFactory");
    portFactory = await Factory.deploy(portController.address);
    await portFactory.deployed();

    return {
        signers, 
        factory, weth, router, tokenData, tokens, pairs,
        portUtils, portController, portFactory 
    };
}

exports.portTokenFixtures = async function () {
    
    const {
        signers,
        factory, weth, router, tokenData, tokens, pairs,
        portUtils, portController, portFactory 
    } = await waffle.loadFixture(exports.portFixtures);

    let intTokenData = tokenData;
    let intTokens = {};
    let intTokenAmounts = {};

    for (key of Object.keys(tokenData)) {
        intTokens[key] = tokens[key];
        intTokenAmounts[key] = ethers.utils.parseUnits("0.5", tokenData[key].dec);
    }

    await portFactory.connect(signers[1]).createPortToken(
        "Test port token",
        "PRT",
        Object.values(intTokens).map(i=>i.address),
        Object.values(intTokenAmounts)
    );


    let PortToken = await ethers.getContractFactory("PortToken");
    let portAddr = await portFactory.portTokens(await portFactory.portTokensLength() - 1);
    let portToken = await PortToken.attach(portAddr);
   
    return {
        signers,
        factory, weth, router, tokenData, tokens, pairs, 
        portUtils, portController, portFactory,
        portAddr, portToken, intTokenData, intTokens, intTokenAmounts 
    };
}