const { expect } = require("chai");
const { BigNumber } = require("ethers");
const { waffle } = require("hardhat");
const provider = waffle.provider;

describe("PortToken", function () {
    // let factory;\
    const testCap=BigNumber.from("10").pow(18).mul(100);
    let baseToken;
    let otherTokens = [];
    let signers;


    this.beforeEach(async function () {
        signers = await ethers.getSigners();

        otherTokensContract = await ethers.getContractFactory("TestToken");
        for (i = 0; i < 2; i++) {
            otherTokens.push(await otherTokensContract.connect(signers[2]).deploy("Other token " + i, "OT" + i, testCap));
        }
    });


    it("Deposit and Withdraw", async function () {
        controller = signers[0];
        author = signers[1];
        user = signers[2];
        otherUser = signers[3];
        testNum = BigNumber.from("10").pow(18);
        
        PortToken = await ethers.getContractFactory("PortToken");
        portToken = await PortToken.deploy(
            controller.address,
            author.address,
            "Test port",
            "PRT",
            [otherTokens[0].address, otherTokens[1].address],
            [BigNumber.from("5").pow(18), BigNumber.from("5").pow(18)]
        )
        await portToken.deployed()

        await expect(portToken.connect(user).mint(user.address, testNum)).to.be.revertedWith('CONTROLLER CALL ONLY');
        await expect(portToken.connect(user).burn(user.address, testNum)).to.be.revertedWith('CONTROLLER CALL ONLY');
        
        expect( await portToken.balanceOf(user.address)).to.eq(0);
        
        await portToken.mint(user.address, testNum);
        expect( await portToken.balanceOf(user.address)).to.eq(testNum);

        await portToken.burn(user.address, testNum);
        expect( await portToken.balanceOf(user.address)).to.eq(0);

        await otherTokens[0].connect(user).transfer(portToken.address,testNum);
        expect( await otherTokens[0].balanceOf(portToken.address)).to.eq(testNum);

        await portToken.transferIntToken(otherTokens[0].address, otherUser.address, testNum);
        expect( await otherTokens[0].balanceOf(portToken.address)).to.eq(0);
        expect( await otherTokens[0].balanceOf(otherUser.address)).to.eq(testNum);

        await expect(portToken.transferIntToken(otherTokens[0].address, otherUser.address, testNum)).to.be.reverted;
    });


});