const { expect } = require("chai");
const { waffle } = require("hardhat");
const { BigNumber } = require("ethers");
const provider = waffle.provider;
const { portTokenFixtures } = require("./fixtures/port.js");

describe("PortController", function () {
    let signers;
    let factory, weth, router, tokenData, tokens, pairs;
    let portUtils, portController, portFactory;
    let portAddr, portToken, intTokenData, intTokens, intTokenAmounts;
    let author, user;

    this.beforeEach(async function () {
        ({
            signers,
            factory, weth, router, tokenData, tokens, pairs,
            portUtils, portController, portFactory,
            portAddr, portToken, intTokenData, intTokens, intTokenAmounts
        } = await waffle.loadFixture(portTokenFixtures));

        author = signers[1];
        user = signers[2];
    });


    describe("Basic issue and redeem", function () {
        it("Should issue and redeem", async function () {
            let amt = "10";
            let amtUnits = ethers.utils.parseUnits(amt, await portToken.decimals());
            let balances = {};

            for ([key, token] of Object.entries(tokens)) {
                balances[key] = await token.balanceOf(await user.getAddress());
                await token.connect(user).approve(portController.address, intTokenAmounts[key].mul(amt));
            }


            await expect(
                () => portController.connect(user).issue(portAddr, amtUnits),
                "port token balance increase by issued amount"
            ).to.changeTokenBalance(
                portToken, user, amtUnits
            );


            for ([key, token] of Object.entries(tokens)) {
                expect(
                    await token.balanceOf(await user.getAddress()),
                    "int token balances decrease by taken amount"
                ).to.be.eq(
                    balances[key].sub(intTokenAmounts[key].mul(amt))
                );
            }

            await expect(
                () => portController.connect(user).redeem(portAddr, amtUnits),
                "port token balance decrease by issued amount"
            ).to.changeTokenBalance(
                portToken, user, amtUnits.mul(-1)
            );

            for ([key, token] of Object.entries(tokens)) {
                expect(
                    await token.balanceOf(await user.getAddress()),
                    "int token balances increase by withdrawn amount"
                ).to.be.eq(
                    balances[key]
                );
            }
        });
    });

    describe("FromOtherToken", async function () {
        it("Should fail to issue if not approved", async function () {
            expect(
                await tokens["USDC"].allowance(await user.getAddress(), portController.address),
                "other token allowance to be 0"
            ).to.eq(0);

            await expect(
                portController.connect(user).issueFromOtherToken(portAddr, tokens["USDC"].address, ethers.utils.parseUnits("1", 18), 0)
            ).to.be.revertedWith("ERC20: transfer amount exceeds allowance");
        })

        it("Should fail to redeem if not approved", async function () {
            expect(
                await portToken.allowance(await user.getAddress(), portController.address),
                "port token allowance to be 0"
            ).to.eq(0);

            await expect(
                portController.connect(user).redeemToOtherToken(portAddr, tokens["USDC"].address, ethers.utils.parseUnits("1", 18), 0)
            ).to.be.revertedWith("PC:INSUFFICIENT_ALLOWANCE");
        })


        it("Should issue and redeem port tokens", async function () {
            let amt = "10";
            let amtUnits = ethers.utils.parseUnits(amt, await portToken.decimals());

            let startUSDC = await tokens["USDC"].balanceOf(await user.getAddress());
            let neededUSDC = (await portUtils.getOtherTokenAmount(
                portAddr,
                amtUnits,
                tokens["USDC"].address,
                router.address,
                false
            ))[0];

            await tokens["USDC"].connect(user).approve(portController.address, neededUSDC);
            await expect(
                () => portController.connect(user).issueFromOtherToken(portAddr, tokens["USDC"].address, amtUnits, 0),
                "to issue port tokens"
            ).to.changeTokenBalance(portToken, user, amtUnits);
            expect(
                await tokens["USDC"].balanceOf(await user.getAddress())
            ).to.eq(startUSDC.sub(neededUSDC));


            let returnedUSDC = (await portUtils.getOtherTokenAmount(
                portAddr,
                amtUnits,
                tokens["USDC"].address,
                router.address,
                true
            ))[0];

            await portToken.connect(user).approve(portController.address, amtUnits);
            await expect(
                () => portController.connect(user).redeemToOtherToken(portAddr, tokens["USDC"].address, amtUnits, 0),
                "to redeem port tokens"
            ).to.changeTokenBalance(portToken, user, amtUnits.mul(-1));

            expect(
                await tokens["USDC"].balanceOf(await user.getAddress())
            ).to.eq(startUSDC.sub(neededUSDC).add(returnedUSDC));
        });


    });
    describe("FromWrappedToken", async function () {
        let wrappedToken;
        this.beforeEach(async function () {
            wrappedToken = await (
                await ethers.getContractFactory("WrappedToken")
            ).deploy("Fintropy", "Fint", 6, tokens["USDC"].address);
            await wrappedToken.deployed();
        });

        it("Should fail to issue if not approved or no balance", async function () {
            expect(
                await wrappedToken.allowance(await user.getAddress(), portController.address),
                "wrapped token allowance to be 0"
            ).to.eq(0);

            await expect(
                portController.connect(user).issueFromWrappedToken(portAddr, wrappedToken.address, ethers.utils.parseUnits("1", 18), 0)
            ).to.be.revertedWith("ERC20: transfer amount exceeds balance");

            let startUSDC = await tokens["USDC"].balanceOf(await user.getAddress());
            await tokens["USDC"].connect(user).approve(wrappedToken.address, startUSDC);
            await wrappedToken.connect(user).deposit(startUSDC);

            await expect(
                portController.connect(user).issueFromWrappedToken(portAddr, wrappedToken.address, ethers.utils.parseUnits("1", 18), 0)
            ).to.be.revertedWith("ERC20: transfer amount exceeds allowance");
        })

        it("Should fail to redeem if not approved", async function () {
            expect(
                await portToken.allowance(await user.getAddress(), portController.address),
                "port token allowance to be 0"
            ).to.eq(0);

            await expect(
                portController.connect(user).redeemToWrappedToken(portAddr, wrappedToken.address, ethers.utils.parseUnits("1", 18), 0)
            ).to.be.revertedWith("PC:INSUFFICIENT_ALLOWANCE");
        })


        it("Should issue and redeem port tokens", async function () {
            let amt = "10";
            let amtUnits = ethers.utils.parseUnits(amt, await portToken.decimals());

            let startUSDC = await tokens["USDC"].balanceOf(await user.getAddress());
            let neededUSDC = (await portUtils.getOtherTokenAmount(
                portAddr,
                amtUnits,
                tokens["USDC"].address,
                router.address,
                false
            ))[0];
            await tokens["USDC"].connect(user).approve(wrappedToken.address, neededUSDC);
            await wrappedToken.connect(user).deposit(neededUSDC);
            await wrappedToken.connect(user).approve(portController.address, neededUSDC);

            await expect(
                () => portController.connect(user).issueFromWrappedToken(portAddr, wrappedToken.address, amtUnits, 0),
                "to issue port tokens"
            ).to.changeTokenBalance(portToken, user, amtUnits);
            expect(
                await wrappedToken.balanceOf(await user.getAddress())
            ).to.eq(0);


            let returnedUSDC = (await portUtils.getOtherTokenAmount(
                portAddr,
                amtUnits,
                tokens["USDC"].address,
                router.address,
                true
            ))[0];

            await portToken.connect(user).approve(portController.address, amtUnits);
            await expect(
                () => portController.connect(user).redeemToWrappedToken(portAddr, wrappedToken.address, amtUnits, 0),
                "to redeem port tokens"
            ).to.changeTokenBalance(portToken, user, amtUnits.mul(-1));

            expect(
                await wrappedToken.balanceOf(await user.getAddress())
            ).to.eq(returnedUSDC);
        });


    });




});