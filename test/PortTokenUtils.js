const { expect } = require("chai");
const { BigNumber } = require("ethers");
const { waffle, ethers } = require("hardhat");
const provider = waffle.provider;
const { portTokenFixtures } = require("./fixtures/port.js");

describe("PortTokenUtils", function () {
    let signers;
    let factory, weth, router, tokenData, tokens, pairs;
    let portUtils, portController, portFactory;
    let portAddr, portToken, intTokenData, intTokens, intTokenAmounts;


    this.beforeEach(async function () {
        ({
            signers,
            factory, weth, router, tokenData, tokens, pairs,
            portUtils, portController, portFactory,
            portAddr, portToken, intTokenData, intTokens, intTokenAmounts
        } = await waffle.loadFixture(portTokenFixtures));
    });

    describe("getIntAmount", function () {
        it("Should calculate proportions", async function () {
            let amt = "0.77";
            let amtUnits = ethers.utils.parseUnits(amt, await portToken.decimals());

            let [resTokens, resQuantity] = await portUtils.getIntAmount(
                portAddr,
                amtUnits
            );

            expect(resTokens).deep.to.eq(Object.values(intTokens).map(i => i.address));
            expect(resQuantity).deep.to.eq(Object.values(intTokenAmounts).map(i => i.mul(amtUnits).div(
                ethers.utils.parseUnits("1", 18)
            )));
        });
    });

    describe("getOtherTokenAmount", function () {
        it("Should estimate swap prices", async function () {
            let amt = "0.77";
            let amtUnits = ethers.utils.parseUnits(amt, await portToken.decimals());

            let [outRes, outDetails] = await portUtils.getOtherTokenAmount(
                portAddr,
                amtUnits,
                tokens["USDC"].address,
                router.address,
                true
            );

            let outRefRes = BigNumber.from(0);
            let outRefDetails = [];
            for ([key, token] of Object.entries(intTokens)) {
                let ref;
                if (key == "USDC") {
                    ref = intTokenAmounts[key].mul(amtUnits).div(ethers.utils.parseUnits("1", 18))
                } else {
                    ref = (await router.getAmountsOut(
                        intTokenAmounts[key].mul(amtUnits).div(ethers.utils.parseUnits("1", 18)),
                        [token.address, tokens["USDC"].address]
                    ))[1];
                }

                outRefRes = outRefRes.add(ref);
                outRefDetails.push(ref);
            }
            expect(outRes).to.be.eq(outRefRes);
            expect(outDetails).to.deep.eq(outRefDetails);


            let [inRes, inDetails] = await portUtils.getOtherTokenAmount(
                portAddr,
                amtUnits,
                tokens["USDC"].address,
                router.address,
                false
            );

            let inRefRes = BigNumber.from(0);
            let inRefDetails = [];
            for ([key, token] of Object.entries(intTokens)) {
                let ref;
                if (key == "USDC") {
                    ref = intTokenAmounts[key].mul(amtUnits).div(ethers.utils.parseUnits("1", 18))
                } else {
                    ref = (await router.getAmountsIn(
                        intTokenAmounts[key].mul(amtUnits).div(ethers.utils.parseUnits("1", 18)),
                        [tokens["USDC"].address, token.address]
                    ))[0];
                }

                inRefRes = inRefRes.add(ref);
                inRefDetails.push(ref);
            }
            expect(inRes).to.be.eq(inRefRes);
            expect(inDetails).to.deep.eq(inRefDetails);
        });
    });
});