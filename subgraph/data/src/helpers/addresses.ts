import { Address } from '@graphprotocol/graph-ts'

export let ZERO_ADDRESS = Address.fromHexString("0x0000000000000000000000000000000000000000")
