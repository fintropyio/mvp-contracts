import { PortToken } from '../../generated/schema'

import { Transfer } from '../../generated/templates/PortTokenSource/PortTokenSource'

import { ZERO_ADDRESS } from '../helpers/addresses'
// import { log } from '@graphprotocol/graph-ts'

import {
  decreaseAccountBalance,
  getOrCreateAccount,
  increaseAccountBalance,
} from './account'

export function handleTransfer(event: Transfer): void {

  let portToken = PortToken.load(event.address.toHex())

  let amount = event.params.value

  if (event.params.from == ZERO_ADDRESS) {

    let destinationAccount = getOrCreateAccount(event.params.to)
    let destinationAccountBalance = increaseAccountBalance(destinationAccount, portToken as PortToken, amount)

    portToken.totalSupply = portToken.totalSupply.plus(amount)

    portToken.totalMinted = portToken.totalMinted.plus(amount)

    destinationAccount.save()
    destinationAccountBalance.save()

  } else if (event.params.to == ZERO_ADDRESS) {

    let sourceAccount = getOrCreateAccount(event.params.from)
    let sourceAccountBalance = decreaseAccountBalance(sourceAccount, portToken as PortToken, amount)

    portToken.totalSupply = portToken.totalSupply.minus(amount)

    portToken.totalBurned = portToken.totalBurned.plus(amount)

    sourceAccount.save()
    sourceAccountBalance.save()

  } else {

    let sourceAccount = getOrCreateAccount(event.params.from)
    let destinationAccount = getOrCreateAccount(event.params.to)

    let sourceAccountBalance = decreaseAccountBalance(sourceAccount, portToken as PortToken, amount)
    let destinationAccountBalance = increaseAccountBalance(destinationAccount, portToken as PortToken, amount)

    portToken.totalTransferred = portToken.totalTransferred.plus(amount)

    sourceAccount.save()
    sourceAccountBalance.save()
    destinationAccount.save()
    destinationAccountBalance.save()

  }

  portToken.save()

}