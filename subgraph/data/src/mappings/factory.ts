import { ZERO } from '../helpers/number'

import { PortToken } from '../../generated/schema'

import { PortTokenSource } from '../../generated/templates'
import { CreatedPortToken } from '../../generated/PortFactory/PortFactory'

import { Address, Bytes, log } from '@graphprotocol/graph-ts'


import {
  getOrCreateAccount
} from './account'

export function handleCreatedPortToken(event: CreatedPortToken): void {

  let portTokenId = event.params.tokenAddr.toHex()

  let portToken = new PortToken(portTokenId)

  let managerAccount = getOrCreateAccount(event.params.manager)

  portToken.transactionHash = event.transaction.hash

  portToken.address = Address.fromString(portTokenId)
  portToken.name = event.params.name
  portToken.symbol = event.params.symbol
  portToken.manager = managerAccount.address

  portToken.decimals = 18
  portToken.totalSupply = ZERO
  portToken.totalBurned = ZERO
  portToken.totalMinted = ZERO
  portToken.totalTransferred = ZERO

  portToken.internalTokenAmounts = event.params.internalTokenAmounts

  let numIntTokens = event.params.internalTokenAmounts.length

  let internalTokenAddresses =  new Array<Bytes>(numIntTokens)
  let addresses = event.params.internalTokenAddresses
  for (let i = 0; i < numIntTokens; i += 1) {
    internalTokenAddresses[i] = Address.fromString(addresses[i].toHexString())
  }

  portToken.internalTokenAddresses = internalTokenAddresses

  managerAccount.save()
  portToken.save()

  PortTokenSource.create(Address.fromString(portTokenId))
}

