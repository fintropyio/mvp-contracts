const fs = require('fs')
const path = require('path')
const colors = require('colors')

async function main() {
  console.log()
  console.log('                                                                     '.bgWhite.black.bold)
  console.log('       Fintropy subgraph ABI update                                  '.bgWhite.black.bold)
  console.log('                                                                     '.bgWhite.black.bold)
  console.log()

  const artifacts_dir = path.resolve('artifacts/contracts')
  const graph_abi_dir = path.resolve('subgraph/data/abis')

  const contract_names = [
    'PortFactory',
    'PortToken',
  ]

  contract_names.forEach(name => {
    let abi = require(path.join(artifacts_dir, `${name}.sol`, `${name}.json`)).abi
    let abi_str = JSON.stringify(abi, null, 2)
    const out_path = path.join(graph_abi_dir, `${name}.json`)
    fs.writeFileSync(out_path, abi_str, 'utf-8')
    console.log('saved', name.bgBlack.brightGreen.bold,  'abi to', out_path.bgBlack.brightGreen.bold)
    console.log()
  })

}


main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
