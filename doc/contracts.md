```mermaid
sequenceDiagram
    participant Owner
    participant Client
    participant  Contract
    
    Owner->>Contract: Construct <br>(baseTokenAddr, internalTokenAddrs, tokenShares)
    
    Client->>+Contract: Deposit<br>(amount)
    Contract-->>-Client: portfolio tokens

    Client->>+Contract: Withdrawal<br>(amount)
    Contract-->>-Client: base tokens

    Client->>+Contract: WithdrawalContents<br>(amount)
    Contract-->>-Client: internal tokens

```