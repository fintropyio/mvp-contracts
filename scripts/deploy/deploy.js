const fs = require('fs')
const path = require('path')
const colors = require('colors')

const { ethers, network } = require('hardhat')

const network_name = network.name

process.env.NODE_CONFIG_ENV = network_name
process.env.NODE_ENV = "production"
var config = require('config')


async function main() {
  console.log()
  console.log('                                                                     '.bgWhite.black.bold)
  console.log('       Fintropy deployment & configuration                           '.bgWhite.black.bold)
  console.log('                                                                     '.bgWhite.black.bold)
  console.log()

  const in_config_dir = path.resolve('config')
  const out_config_dir = path.resolve('frontend/config')
  const artifacts_dir = path.resolve('artifacts/contracts')
  let out_values = {}
  
  let rpc_url = config.provider_rpc + (network_name.includes('fork') ? '' : config.infura_id)
  console.log('rpc_url', rpc_url.bgBlack.blue.bold)
  console.log()
  console.log('subgraph endpoint', config.subgraph_endpoint.bgBlack.blue.bold)
  console.log()

  let provider = new ethers.providers.JsonRpcProvider(rpc_url)
  let network = await provider.getNetwork()

  console.log('network', `${network_name}`.bgBlack.brightYellow.bold, 'chainId', `${network.chainId}`.bgBlack.brightYellow.bold)
  console.log()

  if (config.host_private_key.length != '64') throw new Error('FINT_HOST_PRIVATE_KEY env variable is not defined')
  var signer = new ethers.Wallet(config.host_private_key, provider)

  out_values.NETWORK = network_name
  out_values.INFURA_ID = config.infura_id
  out_values.CHAIN_ID = network.chainId
  out_values.OWNER_ADDRESS = signer.address
  out_values.OWNER_PRIVATE_KEY = config.host_private_key
  out_values.SWAP_ROUTER_ADDRESS = config.swap_router_address
  out_values.SUBGRAPH_ENDPOINT = config.subgraph_endpoint

  console.log('host', signer.address.bgBlack.brightGreen.bold)
  console.log('host balance', ethers.utils.formatUnits(await signer.getBalance(), '18').bgBlack.brightGreen.bold)
  console.log()

  // ---------------------------------------------------------------------------------------------------
  // ---------------- Contracts ------------------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------------

  console.log('       Contracts                                                     '.bgWhite.black.bold)
  console.log()

  // ---------------- PortTokenUtils -------------------------------------------------------------------

  let PortTokenUtils = (await ethers.getContractFactory("PortTokenUtils")).connect(signer)
  let port_token_utils_address = config.contracts_addressess.PortTokenUtils
  if (!port_token_utils_address) {
    console.log('PortTokenUtils'.bgBlack.brightYellow.bold, 'address is not defined. Deploying ...')
    let port_token_utils = await(await PortTokenUtils.deploy()).deployed()
    port_token_utils_address = port_token_utils.address
  }
  console.log('PortTokenUtils'.bgBlack.brightYellow.bold, port_token_utils_address.bgBlack.brightGreen.bold)
  console.log()

  // ---------------- PortController -------------------------------------------------------------------

  let PortController = (await ethers.getContractFactory("PortController", {
    libraries: {
      PortTokenUtils: port_token_utils_address
    }
  })).connect(signer)
  let port_controller_address = config.contracts_addressess.PortController
  if (!port_controller_address) {
    console.log('PortController'.bgBlack.brightYellow.bold, 'address is not defined. Deploying ...')
    let port_controller = await(await PortController.deploy(out_values.SWAP_ROUTER_ADDRESS)).deployed()
    port_controller_address = port_controller.address
  }
  console.log('PortController'.bgBlack.brightYellow.bold, port_controller_address.bgBlack.brightGreen.bold)
  console.log()

  // ---------------- PortFactory ----------------------------------------------------------------------

  let PortFactory = (await ethers.getContractFactory("PortFactory")).connect(signer)
  let port_factory_address = config.contracts_addressess.PortFactory
  if (!port_factory_address) {
    console.log('PortFactory'.bgBlack.brightYellow.bold, 'address is not defined. Deploying ...')
    let port_factory = await(await PortFactory.deploy(port_controller_address)).deployed()
    port_factory_address = port_factory.address
  }
  console.log('PortFactory'.bgBlack.brightYellow.bold, port_factory_address.bgBlack.brightGreen.bold)
  console.log()

  out_values.PORT_TOKEN_UTILS_ADDRESS = port_token_utils_address
  out_values.PORT_CONTROLLER_ADDRESS = port_controller_address
  out_values.PORT_FACTORY_ADDRESS = port_factory_address

  out_values.ABI = {
    'PortFactory': require(path.join(artifacts_dir, 'PortFactory.sol', 'PortFactory.json')).abi,
    'PortController': require(path.join(artifacts_dir, 'PortController.sol', 'PortController.json')).abi,
    'PortToken': require(path.join(artifacts_dir, 'PortToken.sol', 'PortToken.json')).abi,
    'PortTokenUtils': require(path.join(artifacts_dir, 'PortTokenUtils.sol', 'PortTokenUtils.json')).abi,
    'VanilaERC20': require(path.join(artifacts_dir, 'VanilaERC20.sol', 'VanilaERC20.json')).abi,
    'WrappedToken': require(path.join(artifacts_dir, 'WrappedToken.sol', 'WrappedToken.json')).abi,
  }

  // -------------------------------------------------------------------------------------------------
  // ---------------- Tokens -------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------

  out_values.TOKENS = []
  let wrappedTokens = []

  if (config.tokens.length > 0) {
    console.log('       Tokens                                                        '.bgWhite.black.bold)
    console.log()

    out_values.FAUCET_BASE_TOKEN_AMOUNT = '0.05'
    out_values.FAUCET_BASE_TOKEN_DECIMALS = '18'
    out_values.FAUCET_TOKEN_AMOUNT = '10'
    out_values.FAUCET_TOKEN_DECIMALS = '18'

    for (const t of config.tokens) {
      let wt = null

      if (!t.address) {
        console.log(t.name.bgBlack.brightYellow.bold, 'address is not defined. Deploying ...')
        
        let wrap = async (wrappedName, wrappedSymbol, wrappedDecimals, baseTokenAddress, wrappedSupply=null, baseToken=null) => {
          console.log('Wrapping ...')
  
          let WrappedToken = (await ethers.getContractFactory('WrappedToken')).connect(signer)
          let wrappedToken = await (await WrappedToken.deploy(wrappedName, wrappedSymbol, parseInt(wrappedDecimals), baseTokenAddress)).deployed()
          
          if (wrappedSupply && baseToken) {
            console.log('Depositing...')
            await (await baseToken.approve(wrappedToken.address, wrappedSupply)).wait()
            await (await wrappedToken.deposit(wrappedSupply)).wait()
          }

          return {
            address: wrappedToken.address,
            base_token_address: baseTokenAddress,
            name: wrappedName,
            symbol: wrappedSymbol,
            abi: 'WrappedToken',
            supply: wrappedSupply ? ethers.utils.formatUnits(wrappedSupply, wrappedDecimals) : 0,
            decimals: wrappedDecimals
          }
        }

        if (!t.base_token_address) {
          let VanilaERC20 = (await ethers.getContractFactory('VanilaERC20')).connect(signer)
          let supply = ethers.utils.parseUnits(t.supply, t.decimals)
          let token = await (await VanilaERC20.deploy(t.name, t.symbol, supply, parseInt(t.decimals))).deployed()
  
          if (config.wrap_deployed) {
            wt = Object.assign({}, t, await wrap(
              'Wrapped ' + t.name,
              'W' + t.symbol,
              t.decimals, 
              token.address,
              supply.div(2)
            ))
            wrappedTokens.push(wt)
          }
          t.address = token.address
        } else {
          wt = Object.assign({}, t, await wrap(
            t.name,
            t.symbol,
            t.decimals, 
            t.base_token_address
          ))
          wrappedTokens.push(wt)
        }
      }
      out_values.TOKENS.push(t)

      console.log(t.name.bgBlack.brightYellow.bold, t.address.bgBlack.brightGreen.bold)
      if (wt) console.log(wt.name.bgBlack.brightYellow.bold, wt.address.bgBlack.brightGreen.bold)
      console.log()
    }
  }

  // -------------------------------------------------------------------------------------------------
  // ---------------- Pairs --------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------

  if (out_values.TOKENS.length > 1 && config.deploy_pairs) {
    console.log('       Pairs                                                         '.bgWhite.black.bold)
    console.log()

    let make_token_contract = (address) => {
      return new ethers.Contract(
        address,
        out_values.ABI['VanilaERC20'],
        signer
      )
    }

    if (config.pairs.length < 1) {
      // Create / fetch pairs

      let swap_factory_contract = new ethers.Contract(
        config.swap_factory_address,
        // UniswapV2Factory
        [{"inputs":[{"internalType":"address","name":"_feeToSetter","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"token0","type":"address"},{"indexed":true,"internalType":"address","name":"token1","type":"address"},{"indexed":false,"internalType":"address","name":"pair","type":"address"},{"indexed":false,"internalType":"uint256","name":"","type":"uint256"}],"name":"PairCreated","type":"event"},{"constant":true,"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"allPairs","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"allPairsLength","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"tokenA","type":"address"},{"internalType":"address","name":"tokenB","type":"address"}],"name":"createPair","outputs":[{"internalType":"address","name":"pair","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"feeTo","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"feeToSetter","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"}],"name":"getPair","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_feeTo","type":"address"}],"name":"setFeeTo","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_feeToSetter","type":"address"}],"name":"setFeeToSetter","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"}],
        signer
      )
      let swap_router_contract = new ethers.Contract(
        config.swap_router_address,
        // UniswapV2Router02
        [{"inputs":[{"internalType":"address","name":"_factory","type":"address"},{"internalType":"address","name":"_WETH","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"inputs":[],"name":"WETH","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"tokenA","type":"address"},{"internalType":"address","name":"tokenB","type":"address"},{"internalType":"uint256","name":"amountADesired","type":"uint256"},{"internalType":"uint256","name":"amountBDesired","type":"uint256"},{"internalType":"uint256","name":"amountAMin","type":"uint256"},{"internalType":"uint256","name":"amountBMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"addLiquidity","outputs":[{"internalType":"uint256","name":"amountA","type":"uint256"},{"internalType":"uint256","name":"amountB","type":"uint256"},{"internalType":"uint256","name":"liquidity","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"amountTokenDesired","type":"uint256"},{"internalType":"uint256","name":"amountTokenMin","type":"uint256"},{"internalType":"uint256","name":"amountETHMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"addLiquidityETH","outputs":[{"internalType":"uint256","name":"amountToken","type":"uint256"},{"internalType":"uint256","name":"amountETH","type":"uint256"},{"internalType":"uint256","name":"liquidity","type":"uint256"}],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"factory","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"},{"internalType":"uint256","name":"reserveIn","type":"uint256"},{"internalType":"uint256","name":"reserveOut","type":"uint256"}],"name":"getAmountIn","outputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"reserveIn","type":"uint256"},{"internalType":"uint256","name":"reserveOut","type":"uint256"}],"name":"getAmountOut","outputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"}],"name":"getAmountsIn","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"}],"name":"getAmountsOut","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountA","type":"uint256"},{"internalType":"uint256","name":"reserveA","type":"uint256"},{"internalType":"uint256","name":"reserveB","type":"uint256"}],"name":"quote","outputs":[{"internalType":"uint256","name":"amountB","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"address","name":"tokenA","type":"address"},{"internalType":"address","name":"tokenB","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountAMin","type":"uint256"},{"internalType":"uint256","name":"amountBMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"removeLiquidity","outputs":[{"internalType":"uint256","name":"amountA","type":"uint256"},{"internalType":"uint256","name":"amountB","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountTokenMin","type":"uint256"},{"internalType":"uint256","name":"amountETHMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"removeLiquidityETH","outputs":[{"internalType":"uint256","name":"amountToken","type":"uint256"},{"internalType":"uint256","name":"amountETH","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountTokenMin","type":"uint256"},{"internalType":"uint256","name":"amountETHMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"removeLiquidityETHSupportingFeeOnTransferTokens","outputs":[{"internalType":"uint256","name":"amountETH","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountTokenMin","type":"uint256"},{"internalType":"uint256","name":"amountETHMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"bool","name":"approveMax","type":"bool"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"removeLiquidityETHWithPermit","outputs":[{"internalType":"uint256","name":"amountToken","type":"uint256"},{"internalType":"uint256","name":"amountETH","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountTokenMin","type":"uint256"},{"internalType":"uint256","name":"amountETHMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"bool","name":"approveMax","type":"bool"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"removeLiquidityETHWithPermitSupportingFeeOnTransferTokens","outputs":[{"internalType":"uint256","name":"amountETH","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"tokenA","type":"address"},{"internalType":"address","name":"tokenB","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountAMin","type":"uint256"},{"internalType":"uint256","name":"amountBMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"bool","name":"approveMax","type":"bool"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"removeLiquidityWithPermit","outputs":[{"internalType":"uint256","name":"amountA","type":"uint256"},{"internalType":"uint256","name":"amountB","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapETHForExactTokens","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactETHForTokens","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactETHForTokensSupportingFeeOnTransferTokens","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactTokensForETH","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactTokensForETHSupportingFeeOnTransferTokens","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactTokensForTokens","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactTokensForTokensSupportingFeeOnTransferTokens","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"},{"internalType":"uint256","name":"amountInMax","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapTokensForExactETH","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"},{"internalType":"uint256","name":"amountInMax","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapTokensForExactTokens","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"nonpayable","type":"function"},{"stateMutability":"payable","type":"receive"}],
        signer
      )

      // Make token combinations
      var pairs = out_values.TOKENS.flatMap(
        (a, i) => out_values.TOKENS.slice(i + 1).map(b => {
          return {
            address: null,
            token_a_address: a.address,
            token_a_symbol: a.symbol,
            token_a_initial_amount: ethers.utils.parseUnits(a.pair_amount, a.decimals),
            token_b_address: b.address,
            token_b_symbol: b.symbol,
            token_b_initial_amount: ethers.utils.parseUnits(b.pair_amount, b.decimals),
          }
        })
      )

      for (const p of pairs) {
        const pair_name = `${p.token_a_symbol} ${p.token_b_symbol}`

        let pair_address = null
        let existing_pair_address = await swap_factory_contract.getPair(p.token_a_address, p.token_b_address)

        if (existing_pair_address == ethers.constants.AddressZero) {
          console.log('Pair', pair_name.bgBlack.brightYellow.bold, 'doesn\'t exist. Creating ...')

          // Create a pair

          await (await swap_factory_contract.createPair(p.token_a_address, p.token_b_address)).wait()

          // Add liquidity from host account

          let tokenA_contract = make_token_contract(p.token_a_address)
          let tokenB_contract = make_token_contract(p.token_b_address)

          await (await tokenA_contract.approve(config.swap_router_address, p.token_a_initial_amount)).wait()
          await (await tokenB_contract.approve(config.swap_router_address, p.token_b_initial_amount)).wait()

          console.log('Adding liquidity to', pair_name.brightYellow.bold, '...')
          console.log()

          await (await swap_router_contract.addLiquidity(
            p.token_a_address,
            p.token_b_address,
            p.token_a_initial_amount,
            p.token_b_initial_amount,
            p.token_a_initial_amount.div(2),
            p.token_b_initial_amount.div(2),
            signer.address,
            ethers.BigNumber.from(Date.now() + 1000 * 60 * 10),
            {
              gasLimit: ethers.BigNumber.from(500000),
            }
          )).wait()

          pair_address = await swap_factory_contract.getPair(p.token_a_address, p.token_b_address)

        } else {

          pair_address = existing_pair_address
        }

        p.address = pair_address
      }

    } else {
      // Use pairs defined in config file

      var pairs = config.pairs
    }

    // Display actual pair reserves
    for (const p of pairs) {
      // continue
      const pair_name = `${p.token_a_symbol} ${p.token_b_symbol}`

      let pair_contract = new ethers.Contract(
        p.address,
        // UniswapV2Pair abi
        [{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"},{"indexed":true,"internalType":"address","name":"to","type":"address"}],"name":"Burn","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"}],"name":"Mint","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0In","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1In","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount0Out","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1Out","type":"uint256"},{"indexed":true,"internalType":"address","name":"to","type":"address"}],"name":"Swap","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint112","name":"reserve0","type":"uint112"},{"indexed":false,"internalType":"uint112","name":"reserve1","type":"uint112"}],"name":"Sync","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"constant":true,"inputs":[],"name":"DOMAIN_SEPARATOR","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"MINIMUM_LIQUIDITY","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"PERMIT_TYPEHASH","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"burn","outputs":[{"internalType":"uint256","name":"amount0","type":"uint256"},{"internalType":"uint256","name":"amount1","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"factory","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getReserves","outputs":[{"internalType":"uint112","name":"_reserve0","type":"uint112"},{"internalType":"uint112","name":"_reserve1","type":"uint112"},{"internalType":"uint32","name":"_blockTimestampLast","type":"uint32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_token0","type":"address"},{"internalType":"address","name":"_token1","type":"address"}],"name":"initialize","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"kLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"mint","outputs":[{"internalType":"uint256","name":"liquidity","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"nonces","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"permit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"price0CumulativeLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"price1CumulativeLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"skim","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"amount0Out","type":"uint256"},{"internalType":"uint256","name":"amount1Out","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"bytes","name":"data","type":"bytes"}],"name":"swap","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"sync","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"token0","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"token1","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"}],
        signer
      )
      
      let reserves = await pair_contract.getReserves()

      let tokenA_contract = make_token_contract(await pair_contract.token0())
      let tokenB_contract = make_token_contract(await pair_contract.token1())

      let tokenA_decimals = (await tokenA_contract.decimals()).toString()
      let tokenB_decimals = (await tokenB_contract.decimals()).toString()

      console.log('Pair', pair_name.bgBlack.brightYellow.bold, (await pair_contract.name()).bgBlack.brightYellow.bold, p.address.bgBlack.brightGreen.bold)
      console.log('token0', (await tokenA_contract.symbol()).bgBlack.brightGreen.bold, 'reserve', ethers.utils.formatUnits(reserves[0], tokenA_decimals).bgBlack.brightGreen.bold, 'decimals', tokenA_decimals.bgBlack.brightGreen.bold)
      console.log('token1', (await tokenB_contract.symbol()).bgBlack.brightGreen.bold, 'reserve', ethers.utils.formatUnits(reserves[1], tokenB_decimals).bgBlack.brightGreen.bold, 'decimals', tokenB_decimals.bgBlack.brightGreen.bold)
      console.log()
    }

    let pairs_str = JSON.stringify(pairs, null, 2)
    const pairs_config_path = path.join(in_config_dir, 'deployed', `${network_name}.pairs.json`)
    fs.writeFileSync(pairs_config_path, pairs_str, 'utf-8')
    console.log('saved pairs to', pairs_config_path.bgBlack.brightGreen.bold)
    console.log()
  }

  for (const wt of wrappedTokens) {
    out_values.TOKENS.push(wt)
  }

  let tokens_str = JSON.stringify(out_values.TOKENS, null, 2)
  const tokens_config_path = path.join(in_config_dir, 'deployed', `${network_name}.tokens.json`)
  fs.writeFileSync(tokens_config_path, tokens_str, 'utf-8')
  console.log('saved tokens to', tokens_config_path.bgBlack.brightGreen.bold)
  console.log()


  // -------------------------------------------------------------------------------------------------
  // ---------------- FintropyWeb3 config ------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------

  console.log('       FintropyWeb3 config                                           '.bgWhite.black.bold)
  console.log()

  const out_config_template = require(path.join(out_config_dir, 'template.js'))
  let template_str = JSON.stringify(out_config_template, null, 2)
  for (const [key, value] of Object.entries(out_values)) {
    const value_str = JSON.stringify(value)
    template_str = template_str
      .replaceAll(`"$${key}"`, value_str)
      .replaceAll(`$${key}`, value_str.replace(/^"|"$/g, ''))
  }
  const out_config_str = JSON.stringify(JSON.parse(template_str), null, 2)

  const config_network_path = path.join(out_config_dir, `config.${network_name}.json`)
  fs.writeFileSync(config_network_path, out_config_str, 'utf-8')
  console.log('saved web3 config to', config_network_path.bgBlack.brightGreen.bold)
  console.log()

  const config_current_path = path.join(out_config_dir, 'config.json')
  fs.writeFileSync(config_current_path, out_config_str, 'utf-8')
  console.log('saved web3 config to', config_current_path.bgBlack.brightGreen.bold)
  console.log()

}


main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
